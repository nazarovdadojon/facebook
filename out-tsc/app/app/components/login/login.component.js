import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { RegisterComponent } from '../register/register.component';
let LoginComponent = class LoginComponent {
    constructor(afAuth, authService, router, matDialog) {
        this.afAuth = afAuth;
        this.authService = authService;
        this.router = router;
        this.matDialog = matDialog;
        this.subs = [];
    }
    ngOnInit() {
        this.subs.push(this.authService.userData.subscribe(data => {
            if (data) {
                this.router.navigateByUrl('/').then();
            }
        }));
    }
    ngOnDestroy() {
        this.subs.map(s => s.unsubscribe());
    }
    login(form) {
        const { email, password } = form.value;
        if (!form.valid) {
            return;
        }
        this.authService.SignIn(email, password);
        form.resetForm();
    }
    openRegister() {
        const dialogRef = this.matDialog.open(RegisterComponent, {
            role: 'dialog',
            height: '480px',
            width: '480px'
        });
        dialogRef.afterClosed().subscribe(result => {
            const { fname, lname, email, password, avatar } = result;
            if (result !== undefined) {
                this.authService.SignUp(email, password, fname, lname, avatar);
            }
            return;
        });
    }
};
LoginComponent = __decorate([
    Component({
        selector: 'app-login',
        templateUrl: './login.component.html',
        styleUrls: ['./login.component.scss']
    })
], LoginComponent);
export { LoginComponent };
//# sourceMappingURL=login.component.js.map