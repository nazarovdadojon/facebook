import { __awaiter, __decorate } from "tslib";
import { Component } from '@angular/core';
let HomeComponent = class HomeComponent {
    constructor(postService, authService) {
        this.postService = postService;
        this.authService = authService;
        this.images = [
            'https://images-na.ssl-images-amazon.com/images/I/51DR2KzeGBL._AC_.jpg',
            'https://cdn.pixabay.com/photo/2017/08/30/01/05/milky-way-2695569_960_720.jpg',
            'https://torange.biz/photofx/93/8/light-vivid-colors-fragment-love-background-rain-fall-cover-93412.jpg',
            'https://cdn.pixabay.com/photo/2017/07/18/18/24/dove-2516641_960_720.jpg',
            'https://c0.wallpaperflare.com/preview/956/761/225/5be97da101a3f.jpg',
            'https://upload.wikimedia.org/wikipedia/commons/9/9a/Swepac_FB_465%2C_RV70%2C_with_passing_lorry.jpg'
        ];
        this.subs = [];
        this.posts = [];
    }
    ngOnInit() {
        return __awaiter(this, void 0, void 0, function* () {
            this.subs.push(this.postService.getAllPosts().subscribe((posts) => __awaiter(this, void 0, void 0, function* () {
                this.posts = posts;
                console.log(posts);
            })));
            this.subs.push(this.authService.CurrentUser().subscribe(user => {
                this.user = user;
                console.log(user);
            }));
        });
    }
    postMessage(form) {
        const { message } = form.value;
        this.postService.postMessage(message, `${this.user.firstName} ${this.user.lastName}`, {
            avatar: this.user.avatar,
            lastName: this.user.lastName,
            firstname: this.user.firstName
        });
        form.resetForm();
    }
    logout() {
        this.authService.Logout();
    }
};
HomeComponent = __decorate([
    Component({
        selector: 'app-home',
        templateUrl: './home.component.html',
        styleUrls: ['./home.component.scss']
    })
], HomeComponent);
export { HomeComponent };
//# sourceMappingURL=home.component.js.map