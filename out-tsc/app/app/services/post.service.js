import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase';
let PostService = class PostService {
    constructor(afs, afAuth, authService) {
        this.afs = afs;
        this.afAuth = afAuth;
        this.authService = authService;
        this.afAuth.authState.subscribe(user => this.currentUser = user);
    }
    getAllPosts() {
        return this.afs.collection('posts', ref => ref.orderBy('time', 'desc'))
            .snapshotChanges()
            .pipe(map(actions => {
            return actions.map(item => {
                return Object.assign({ id: item.payload.doc.id }, item.payload.doc.data());
            });
        }));
    }
    postMessage(message, ownerName, otherItem) {
        this.afs.collection('posts').add(Object.assign({ message, title: ownerName, user_id: this.currentUser.uid, time: firebase.firestore.FieldValue.serverTimestamp() }, otherItem)).then(res => console.log(res));
    }
};
PostService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], PostService);
export { PostService };
//# sourceMappingURL=post.service.js.map