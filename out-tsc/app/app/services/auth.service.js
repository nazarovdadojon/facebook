import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
let AuthService = class AuthService {
    constructor(afAuth, router, afs) {
        this.afAuth = afAuth;
        this.router = router;
        this.afs = afs;
        this.currentUser$ = new BehaviorSubject(null);
        this._userData = afAuth.authState;
        this._userData.subscribe(user => {
            if (user) {
                this.afs.collection('users')
                    .doc(user.uid)
                    .valueChanges()
                    .subscribe(currentUser => {
                    this.currentUser = currentUser;
                    this.currentUser$.next(currentUser);
                });
            }
        });
    }
    CurrentUser() {
        return this.currentUser$.asObservable();
    }
    SignUp(email, password, firstName, lastName, avatar = 'https://portal.staralliance.com/cms/aux-pictures/prototype-images/avatar-default.png/@@images/image.png') {
        this.afAuth.createUserWithEmailAndPassword(email, password)
            .then(res => {
            if (res) {
                this.afs.collection('users').doc(res.user.uid)
                    .set({
                    firstName,
                    lastName,
                    email,
                    avatar
                }).then(value => {
                    this.afs.collection('users')
                        .doc(res.user.uid)
                        .valueChanges()
                        .subscribe(user => {
                        console.log(user);
                        if (user) {
                            this.currentUser$.next(user);
                        }
                    });
                });
            }
        })
            .catch(err => console.log(`Something went wrong ${err.message}`));
    }
    get userData() {
        return this._userData;
    }
    SignIn(email, password) {
        console.log(email, password);
        this.afAuth.signInWithEmailAndPassword(email, password)
            .then(res => {
            console.log(res);
            this._userData = this.afAuth.authState;
            this.afs.collection('users')
                .doc(res.user.uid)
                .valueChanges()
                .subscribe((user) => {
                console.log(user);
                // @ts-ignore
                this.currentUser = user;
                this.currentUser$.next(this.currentUser);
            });
        }).catch(err => console.log(err.message));
    }
    Logout() {
        this.afAuth.signOut().then(res => {
            console.log(res);
            this.currentUser = null;
            this.currentUser$.next(this.currentUser);
            this.router.navigateByUrl('/login').then();
        });
    }
    searchUserInDatabase(user_id) {
        return this.afs.collection('users').doc(user_id).valueChanges();
    }
};
AuthService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], AuthService);
export { AuthService };
//# sourceMappingURL=auth.service.js.map