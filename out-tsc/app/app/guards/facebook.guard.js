import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { map, tap } from 'rxjs/operators';
let FacebookGuard = class FacebookGuard {
    constructor(router, afAuth, authService) {
        this.router = router;
        this.afAuth = afAuth;
        this.authService = authService;
    }
    canActivate(next, state) {
        return this.authService.userData
            .pipe(map(user => user !== null), tap(value => {
            if (!value) {
                this.router.navigateByUrl('/login').then();
                return value;
            }
            else {
                return value;
            }
        }));
    }
};
FacebookGuard = __decorate([
    Injectable({
        providedIn: 'root'
    })
], FacebookGuard);
export { FacebookGuard };
//# sourceMappingURL=facebook.guard.js.map